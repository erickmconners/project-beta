# CarCar

Team:

* Helmer Jimenez Jr - Service
* Erick - Sales

## Design

## Service microservice

Made Technican, Automobile VO, and Appointment models to help integrate backend functions that ultimately connect to the fronntend portion (React) to build a car dealership website.

## Sales microservice

Making a sales model with multiple attributes and foreignkeys that will connect to other microservices in the project. I plan on creating restful api's to modify or add or change data, and I'll make front-end React files using Javascript that will gather data from my microservice and show it. Maybe a form or 2 will be made so we can add extra data?

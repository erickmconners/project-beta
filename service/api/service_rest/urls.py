from django.urls import path
from .views import (
    api_list_technicians,
    api_delete_technician,
    api_list_appointments,
    api_delete_appointment,
    api_set_appointment_canceled,
    api_set_appointment_finished,
)
urlpatterns = [
    path("technicians/", api_list_technicians, name="api_list_technician"),
    path("technicians/<int:id>/", api_delete_technician, name="api_delete"),
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/<int:id>/", api_delete_appointment, name="api_delete"),
    path("appointments/<int:id>/cancel/", api_set_appointment_canceled, name="api_cancel"),
    path("appointments/<int:id>/finish/", api_set_appointment_finished, name="api_finished"),
]

from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .encoders import (
    Technician,
    TechnicianListEncoder,
    Appointment,
    AppointmentListEncoder,
)
from django.http import JsonResponse
import json
from django.shortcuts import get_object_or_404


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {"technicians": list(technician.values())},
            safe=False,
            encoder=TechnicianListEncoder
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(
            first_name=content["first_name"],
            last_name=content["last_name"],
            employee_id=content["employee_id"]
        )
        return JsonResponse(
            {"message": "New technician created successfully."},
            status=201
        )


@require_http_methods(["DELETE"])
def api_delete_technician(request, id):
    if request.method == "DELETE":
        technician = get_object_or_404(Technician, id=id)
        technician.delete()
        return JsonResponse(
            {"message": "Technician was deleted successfully."},
            status=201
        )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": list(appointments.values())},
            encoder=AppointmentListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        appointments = Appointment.objects.create(**content)
        return JsonResponse(
            appointments,
            encoder=AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_appointment(request, id):
    if request.method == "DELETE":
        appointments = get_object_or_404(Appointment, id=id)
        appointments.delete()
        return JsonResponse(
            {"message": "Appointment was deleted successfully."},
            status=200,
        )


@require_http_methods(["PUT"])
def api_set_appointment_canceled(request, id):
    appointment = get_object_or_404(Appointment, id=id)
    appointment.status = "canceled"
    appointment.save()
    return JsonResponse(
        {"message": f"Appointment {id} status is set to 'canceled'."},
        status=200,
    )

@require_http_methods(["PUT"])
def api_set_appointment_finished(request, id):
    appointment = get_object_or_404(Appointment, id=id)
    appointment.status = "finished"
    appointment.save()
    return JsonResponse(
        {"message": f"Appointment {id} status is set to 'finished'."},
        status=200,
    )

import React, { useEffect, useState } from 'react';

function ServiceHistoryList() {
  const [appointments, setAppointments] = useState([]);
  const [searchVIN, setSearchVIN] = useState([]);

  const handlseSearchChange = (event) => {
    setSearchVIN(event.target.value);
  };

  const handleSearchSubmit = (event) => {
    event.preventDefault();
    if (searchVIN.trim() === '') {
        fetchServiceHistory();
    } else {
        const filteredAppointments = appointments.filter((appointment) =>
        appointment.vin.toLowerCase().includes(searchVIN.toLowerCase())
    );
    setAppointments(filteredAppointments);

    }
 };



  async function fetchServiceHistory() {
    const response = await fetch('http://localhost:8080/api/appointments/');

    if (response.ok) {
      const parsedJson = await response.json();
      setAppointments(parsedJson.appointments);
    }
  }

  useEffect(() => {
    fetchServiceHistory();
  }, []);

  function formatDate(string) {
    const date = new Date();
    const timeString = date.toLocaleTimeString(undefined, {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
        hour: 'numeric',
        minute: '2-digit'
    });
    return timeString.replace(',', ', ');
}


  return (
    <div>
        <h1>Service History</h1>
    <form onSubmit={handleSearchSubmit} className='input-group mb-3'>
          <input
              type="text"
              value={searchVIN}
              onChange={handlseSearchChange}
              placeholder="Enter VIN to search" />
          <button type="submit" className='btn btn-primary'>Search</button>
        </form>

      <table className="table table-striped">
              <thead>
                  <tr>
                      <th>VIN</th>
                      <th>Is VIP?</th>
                      <th>Customer</th>
                      <th>Date and Time</th>
                      <th>Technician</th>
                      <th>Reason</th>
                      <th>Status</th>
                  </tr>
              </thead>
              <tbody>
                  {appointments.map((appointment) => {
                      return (
                          <tr key={appointment.id}>
                              <td>{appointment.vin}</td>
                              <td>{appointment.is_vip}</td>
                              <td>{appointment.customer}</td>
                              <td>{formatDate(appointment.date_time)}</td>
                              <td>{appointment.technician}</td>
                              <td>{appointment.reason}</td>
                              <td>{appointment.status}</td>
                          </tr>
                      );
                  })}
              </tbody>
          </table>
          </div>
  );
}

export default ServiceHistoryList;

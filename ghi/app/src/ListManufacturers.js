import React, { useState, useEffect } from 'react';

function ListManufacturers() {
    const [manu, setManu] = useState([]);

    useEffect(()=> {
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setManu(data.manufacturers);
            }
        } catch (error) {
            console.log('Fetch Error:', error);
        }
    };
    fetchData();
    }, [])

    return (
        <div>
            <h1>Manufacturers</h1>
        <table className="table table-hover">
            <thead className="table-warning">
                <tr>
                    <th scope="col">Name</th>
                </tr>
            </thead>
            <tbody className="table-group-divider">
                {manu.map(s => {
                    return (
                        <tr key={s.id}>
                            <td>{s.name}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
    );
}

export default ListManufacturers;

import React, { useState, useEffect } from 'react';

function ListSales() {
    const [sales, setSales] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8090/api/sales/';
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                console.log('API Response:', data);
                setSales(data.Sale);
            }
        } catch (error) {
            console.log('Fetch Error:', error);
        }
    };
    useEffect(()=> {
        fetchData();
    }, [])

    return (
        <div>
            <h1>Sales</h1>
        <table className="table table-hover">
            <thead className="table-warning">
                <tr>
                    <th scope="col">Salesperson</th>
                    <th scope="col">Employee ID</th>
                    <th scope="col">Customer</th>
                    <th scope="col">VIN</th>
                    <th scope="col">Price</th>
                </tr>
            </thead>
            <tbody className="table-group-divider">
                {sales.map(sale => {
                    return (
                        <tr key={sale.id}>
                            <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                            <td>{sale.salesperson.employee_id}</td>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
    );
}

export default ListSales;

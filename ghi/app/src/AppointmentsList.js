import React, { useEffect, useState } from 'react';

function AppointmentsList() {
  const [appointments, setAppointments] = useState([]);
  const [soldAutomobiles, setSoldAutomobiles] = useState([]);

  async function fetchAppointments() {
    const response = await fetch('http://localhost:8080/api/appointments/');

    if (response.ok) {
      const parsedJson = await response.json();
      setAppointments(parsedJson.appointments);
    }
  }

  async function fetchSoldAutomobiles() {
    const response = await fetch('http://localhost:8080/api/automobiles/');

    if (response.ok) {
      const parsedJson = await response.json();
      setSoldAutomobiles(parsedJson.automobiles);
    }
  }

  useEffect(() => {
    fetchAppointments();
    fetchSoldAutomobiles();
  }, []);

  function formatDate(string) {
    const date = new Date();
    const timeString = date.toLocaleTimeString(undefined, {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
        hour: 'numeric',
        minute: '2-digit'
    });
    return timeString.replace(',', ', ');
}

async function cancelAppointment(appointmentId) {
  try{
  await fetch(`http://localhost:8080/api/appointments/${appointmentId}/cancel`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
    });

    setAppointments((prevAppointments) =>
        prevAppointments.filter((appointment) => appointment.id !== appointmentId)
      );
    } catch (error) {
      console.error('Error canceling appointment:', error);
    }
  }


async function finishAppointment(appointmentId) {
  try{
  await fetch(`http://localhost:8080/api/appointments/${appointmentId}/finish`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
    });

    setAppointments((prevAppointments) =>
        prevAppointments.filter((appointment) => appointment.id !== appointmentId)
      );
    } catch (error) {
      console.error('Error finalizing appointment:', error);
    }
  }

  return (
    <div>
      <h1>Service Appointments</h1>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Is VIP?</th>
          <th>Customer</th>
          <th>Date and Time</th>
          <th>Technician</th>
          <th>Reason</th>
        </tr>
      </thead>
      <tbody>
        {appointments.map((appointment) => {
          return (
            <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{appointment.vip_status}</td>
              <td>{appointment.customer}</td>
              <td>{formatDate(appointment.date_time)}</td>
              <td>{appointment.technician_id}</td>
              <td>{appointment.reason}</td>

              <td>
              <button type="button" className="btn btn-light" onClick={() => cancelAppointment(appointment.id)}>Cancel</button>
              </td>
              <td>
              <button type="button" className="btn btn-dark" onClick={() => finishAppointment(appointment.id)}>Finish</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
    </div>
  );
}

export default AppointmentsList;

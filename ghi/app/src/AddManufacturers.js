import React, { useState } from 'react';

function AddManufacturers() {
    const [name, setName] = useState('');

    function handleNameChange(e) {
        setName(e.target.value)
    }
    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {
            name: name,
        };
        const response = await fetch('http://localhost:8100/api/manufacturers/', {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'applicatiion/json',
            }
        });
        if (response.ok) {
            setName('')
        }
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                        <div className="form-floating mb-3">
                            <input value={name} onChange={handleNameChange} placeholder="First name" required type="text" name="first_name" className="form-control" />
                            <label htmlFor="firstName">Manufacturer name...</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default AddManufacturers

import React, { useState, useEffect } from 'react';

function ListSalesperson() {
    const [salesperson, setSalesperson] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setSalesperson(data.Salesperson);
            }
        } catch (error) {
            console.log('Fetch Error:', error);
        }
    };
    useEffect(()=> {
        fetchData();
    }, [])

    return (
        <div>
            <h1>Salespersons</h1>
        <table className="table table-hover">
            <thead className="table-warning">
                <tr>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Employee ID</th>
                </tr>
            </thead>
            <tbody className="table-group-divider">
                {salesperson.map(s => {
                    return (
                        <tr key={s.id}>
                            <td>{s.first_name}</td>
                            <td>{s.last_name}</td>
                            <td>{s.employee_id}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
    );
}

export default ListSalesperson;

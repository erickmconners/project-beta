import React, { useEffect, useState } from 'react'

function AddTechnicianForm() {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employee_id: '',
    });

    const getData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
        }
    }

    useEffect(() => {
        getData();
    }, []);


  const handleFormChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const url = 'http://localhost:8080/api/technicians/';
      const fetchConfig = {
        method: 'POST',
        body: JSON.stringify(formData),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        const newTechnician = await response.json();
        console.log(newTechnician);
        alert('Technician created successfully!');
        setFormData({
          first_name: '',
          last_name: '',
          employee_id: '',
        });
      } else {
        console.error('Failed to create technician:', response.status);
        alert('Failed to create technician. Please try again.');
      }
    } catch (error) {
      console.error('Error creating technician:', error);
      alert('An error occurred. Please try again later.');
    }
  };

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new technician</h1>
              <form onSubmit={handleSubmit} id="create-technician-form">
                <div className="form-floating mb-3">
                  <input
                    onChange={handleFormChange}
                    value={formData.first_name}
                    placeholder="First Name"
                    required
                    type="text"
                    name="first_name"
                    id="first_name"
                    className="form-control"
                  />
                  <label htmlFor="first_name">First Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={handleFormChange}
                    value={formData.last_name}
                    placeholder="Last Name"
                    required
                    type="text"
                    name="last_name"
                    id="last_name"
                    className="form-control"
                  />
                  <label htmlFor="last_name">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={handleFormChange}
                    value={formData.employee_id}
                    placeholder="Employee ID"
                    required
                    type="text"
                    name="employee_id"
                    id="employee_id"
                    className="form-control"
                  />
                  <label htmlFor="employee_id">Employee ID</label>
                </div>
                <button className="btn btn-primary">Create Technician</button>
              </form>
            </div>
          </div>
        </div>
      );

    }


export default AddTechnicianForm;

import React, { useEffect, useState } from 'react'

function AddAppointmentForm() {
    const [formData, setFormData] = useState({
        vin: '',
        customer: '',
        date: '',
        time: '',
        technician_id: '',
        reason: '',
    });

    const [technicians, setTechnicians] = useState([]);

    const getData = async () => {
        const url = 'http://localhost:8080/api/appointments/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setFormData(data.appointments)
        }
    }

    const fetchTechnician = async () => {
      try {
        const response = await fetch('http://localhost:8080/api/technicians/');
        if (response.ok) {
          const data = await response.json();
          setTechnicians(data.technicians);
        } else {
          console.error('Failed to fetch technicians:', response.status);
        }
      } catch (error) {
        console.error('Error fetching technicians:', error);
      }
    };

    useEffect(() => {
        getData();
        fetchTechnician();
    }, []);


  const handleFormChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const url = 'http://localhost:8080/api/appointments/';
      const fetchConfig = {
        method: 'POST',
        body: JSON.stringify(formData),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        const newAppointment = await response.json();
        console.log(newAppointment);
        alert('Appointment created successfully!');
        setFormData({
          vin: '',
          customer: '',
          date: '',
          time: '',
          technician_id: '',
          reason: '',
        });
      } else {
        console.error('Failed to create appointment:', response.status);
        alert('Failed to create appointment. Please try again.');
      }
    } catch (error) {
      console.error('Error creating appointmnet:', error);
      alert('An error occurred. Please try again later.');
    }
  };


    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a service appointment</h1>
              <form onSubmit={handleSubmit} id="create-appointment-form">
                <div className="form-floating mb-3">
                  <input
                    onChange={handleFormChange}
                    value={formData.vin}
                    placeholder="Automobile VIN"
                    required
                    type="text"
                    name="vin"
                    id="vin"
                    className="form-control"
                  />
                  <label htmlFor="vin">Automobile VIN</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={handleFormChange}
                    value={formData.customer}
                    placeholder="Customer"
                    required
                    type="text"
                    name="customer"
                    id="customer"
                    className="form-control"
                  />
                  <label htmlFor="customer">Customer</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={handleFormChange}
                    value={formData.date}
                    placeholder="Date"
                    required
                    type="date"
                    name="date"
                    id="date"
                    className="form-control"
                  />
                  <label htmlFor="date">Date</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={handleFormChange}
                    value={formData.time}
                    placeholder="Time"
                    required
                    type="time"
                    name="time"
                    id="time"
                    className="form-control"
                  />
                  <label htmlFor="time">Time</label>
                </div>
                <div className="form-floating mb-3">
              <select
                onChange={handleFormChange}
                value={formData.technician_id}
                name="technician_id"
                id="technician_id"
                className="form-select"
                required
              >
                <option value="">Select a technician</option>
                {technicians.map((technician) => (
                  <option key={technician.id} value={technician.id}>
                    {technician.name}
                  </option>
                ))}
              </select>
              <label htmlFor="technician_id">Technician</label>
            </div>

                <div className="form-floating mb-3">
                  <input
                    onChange={handleFormChange}
                    value={formData.reason}
                    placeholder="Reason"
                    required
                    type="text"
                    name="reason"
                    id="reason"
                    className="form-control"
                  />
                  <label htmlFor="reason">Reason</label>
                </div>
                <button className="btn btn-primary">Create Appointment</button>
              </form>
            </div>
          </div>
        </div>
      );

    }


export default AddAppointmentForm;

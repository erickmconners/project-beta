import React, { useState } from 'react';

function AddCustomer() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [address, setAddress] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');

    function handleFirstChange(e) {
        setFirstName(e.target.value)
    }
    function handleLastChange(e) {
        setLastName(e.target.value)
    }
    function handleAddyChange(e) {
        setAddress(e.target.value)
    }
    function handleNumChange(e) {
        setPhoneNumber(e.target.value)
    }
    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {
            first_name: firstName,
            last_name: lastName,
            address: address,
            phone_number: phoneNumber
        };
        const response = await fetch('http://localhost:8090/api/customers/', {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'applicatiion/json',
            }
        });
        if (response.ok) {
            setFirstName('')
            setLastName('')
            setAddress('')
            setPhoneNumber('')
        }
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Customer</h1>
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                        <div className="form-floating mb-3">
                            <input value={firstName} onChange={handleFirstChange} placeholder="First name" required type="text" name="first_name" className="form-control" />
                            <label htmlFor="firstName">First name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={lastName} onChange={handleLastChange} placeholder="Last name" required type="text" name="last_name" className="form-control" />
                            <label htmlFor="lastName">Last name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={address} onChange={handleAddyChange} placeholder="Employee ID" required type="text" name="employee_id" className="form-control" />
                            <label htmlFor="employeeId">Address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={phoneNumber} onChange={handleNumChange} placeholder="Employee ID" required type="text" name="employee_id" className="form-control" />
                            <label htmlFor="employeeId">Phone Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default AddCustomer

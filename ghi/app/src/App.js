import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Nav';
import MainPage from './MainPage';
import SalespersonHistory from './SalespersonHistory';
import ListManufacturers from './ListManufacturers';
import AddManufacturers from './AddManufacturers';
import ListSalesperson from './ListSalesperson';
import AddSalesperson from './AddSalesperson';
import ListCustomer from './ListCustomer';
import CreateModel from './CreateModel';
import AddCustomer from './AddCustomer';
import ListModels from './ListModels';
import ListSales from './ListSales';
import CreateCar from './CreateCar';
import ListCars from './ListCars';
import NewSale from './NewSale';
import TechnicianList from './TechnicianList';
import AddTechnicianForm from './AddTechnicianForm';
import AppointmentsList from './AppointmentsList';
import AddAppointmentForm from './AddAppointmentForm';
import ServiceHistoryList from './ServiceHistoryList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/salespeople/history" element={<SalespersonHistory />} />
          <Route path="/manufacturers/create" element={<AddManufacturers />} />
          <Route path="/salespeople/create" element={<AddSalesperson />} />
          <Route path="/manufacturers" element={<ListManufacturers />} />
          <Route path="/customers/create" element={<AddCustomer />} />
          <Route path="/automobiles/create" element={<CreateCar />} />
          <Route path="/salespeople" element={<ListSalesperson />} />
          <Route path="/models/create" element={<CreateModel />} />
          <Route path="/customers" element={<ListCustomer />} />
          <Route path="/sales/create" element={<NewSale />} />
          <Route path="/automobiles" element={<ListCars />} />
          <Route path="/models" element={<ListModels />} />
          <Route path="/sales" element={<ListSales />} />
          <Route path="/technicians" element={<TechnicianList />} />
        <Route path="/technicians/create" element={<AddTechnicianForm />} />
        <Route path="/appointments" element={<AppointmentsList />} />
        <Route path="/appointments/create" element={<AddAppointmentForm />} />
        <Route path="/appointments/history" element={<ServiceHistoryList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

import React, { useState, useEffect } from 'react'

function CreateCar() {
    const [color, setColor] = useState('')
    const [year, setYear] = useState('')
    const [vin, setVin] = useState('')
    const [model, setModel] = useState('')
    const [models, setModels] = useState([])


    useEffect(() => {
        const fetchData = async () => {
            const url = 'http://localhost:8100/api/models/'
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setModels(data.models)
            }
        }
    fetchData();
    }, []);

    function handleColorChange(e) {
        setColor(e.target.value)
    }
    function handleYearChange(e) {
        setYear(e.target.value)
    }
    function handleVinChange(e) {
        setVin(e.target.value)
    }
    function handleModelChange(e) {
        setModel(e.target.value)
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const data = {
        color: color,
        year: year,
        vin: vin,
        model_id: model,
        };
        const response = await fetch('http://localhost:8100/api/automobiles/', {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        });
        if (response.ok) {
            setColor('');
            setYear('');
            setVin('');
            setModel('');
        }
    }
    return(
    <div className="row">
    <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Add a Car</h1>
            <form onSubmit={handleSubmit} id="create-car-form">
                <div className="form-floating mb-3">
                    <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="car">Color...</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={year} onChange={handleYearChange} placeholder="Year" required type="text" name="year" id="year" className="form-control" />
                    <label htmlFor="year">Year...</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={vin} onChange={handleVinChange} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                    <label htmlFor="vin">VIN...</label>
                </div>
                <div className="mb-3">
                    <select value={model} onChange={handleModelChange} required name="model" id="model" className="form-select">
                        <option value="">Choose a model...</option>
                        {models.map(m => {
                            return (<option value={m.id} key={m.id}>{m.name}</option>)
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
    </div>
    </div>
)
}
export default CreateCar;

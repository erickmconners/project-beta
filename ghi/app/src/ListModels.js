import React, { useState, useEffect } from 'react';

function ListModels() {
    const [models, setModels] = useState([]);

    useEffect(()=> {
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setModels(data.models);
            }
        } catch (error) {
            console.log('Fetch Error:', error);
        }
    };
    fetchData();
    }, [])

    const cardStyle = {
        width: "18rem",
        margin: "1rem",
    };

    return (
        <>
        <h1>Models</h1>
        <div style={{ display: "flex", flexWrap: "wrap", justifyContent: "flex-start" }}>

        {models.map((s) => (
            <div key={s.id} className="card" style={cardStyle}>
              <img src={s.picture_url} className="card-img-top" alt="..." />
              <div className="card-body">
                <h5 className="card-title">{s.name}</h5>
                <ul className="list-group list-group-flush">
                  <li className="list-group-item">
                    Manufacturer: {s.manufacturer.name}
                  </li>
                </ul>
              </div>
            </div>
          ))}
        </div>
        </>
      );
    };

export default ListModels;

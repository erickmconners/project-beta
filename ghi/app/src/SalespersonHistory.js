import React, { useState, useEffect } from 'react';

function SalespersonHistory() {
  const [sales, setSales] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [theirSales, setTheirSales] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
          const data = await response.json();
          setSales(data.Sale);
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
      };
      fetchData();
      if (salespeople) {
        const theirSales = sales.filter(
          (sale) => sale.salesperson.first_name === salespeople);
        setTheirSales(theirSales);
      } else {
        setTheirSales([]);
      }
    }, [salespeople, sales]);


    return (
      <div className="parent">
      <h1> Salesperson History</h1>
      <div className="btn-group dropend">
        <button type="button" className="btn btn-warning dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
          Salespeople 👨‍💼
        </button>
          <ul className="dropdown-menu">
            {sales.map((sale) => (
              <li
                key={sale.salesperson.first_name}
                onClick={() => setSalespeople(sale.salesperson.first_name)}
              >
                <div className="dropdown-item">{sale.salesperson.first_name} {sale.salesperson.last_name}</div>
              </li>
            ))}
          </ul>
        </div>

      <table className="table table-hover">
        <thead className="table-warning">
              <tr>
                <th scope="col">Salesperson</th>
                <th scope="col">Customer</th>
                <th scope="col">VIN</th>
                <th scope="col">Price</th>
              </tr>
          </thead>
          <tbody className="table-group-divider">
            {theirSales.map((s) => (
              <tr key={s.id}>
                <td>{s.salesperson.first_name} {s.salesperson.last_name}</td>
                <td>{s.customer.first_name} {s.customer.last_name}</td>
                <td>{s.automobile.vin}</td>
                <td>{s.price}</td>
              </tr>
              ))}
          </tbody>
      </table>
      </div>
  );
}
  export default SalespersonHistory;

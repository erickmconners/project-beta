import { NavLink } from 'react-router-dom';
import './index.css';
import React from 'react';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
              Salespersons
            </button>
            <ul className="dropdown-menu">
              <li><a className="dropdown-item" href="/salespeople">Salespersons</a></li>
              <li><a className="dropdown-item" href="/salespeople/create">Add Salesperson</a></li>
              <li><a className="dropdown-item" href="/salespeople/history">Salesperson History</a></li>
            </ul>
          </div>
          <div className="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
              Sales
            </button>
            <ul className="dropdown-menu">
              <li><a className="dropdown-item" href="/sales">Sales</a></li>
              <li><a className="dropdown-item" href="/sales/create">Sales Form</a></li>
            </ul>
          </div>
          <div className="dropdown">
            <button className="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
              Customers
            </button>
            <ul className="dropdown-menu">
              <li><a className="dropdown-item" href="/customers">Customers</a></li>
              <li><a className="dropdown-item" href="/customers/create">Customers Form</a></li>
            </ul>
          </div>
          <div className="dropdown">
            <button className="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
            Technicians
            </button>
            <ul className="dropdown-menu">
              <li><a className="dropdown-item" href="/technicians">Technicians</a></li>
              <li><a className="dropdown-item" href="/technicians/create">Technicians Form</a></li>
            </ul>
          </div>
          <div className="dropdown">
            <button className="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
            Services
            </button>
            <ul className="dropdown-menu">
              <li><a className="dropdown-item" href="/appointments">Service Appointments</a></li>
              <li><a className="dropdown-item" href="/appointments/create">Create Appointment</a></li>
              <li><a className="dropdown-item" href="/appointments/history">Service History</a></li>
            </ul>
          </div>
          <div className="dropdown">
            <button className="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
            Manufacturers
            </button>
            <ul className="dropdown-menu">
              <li><a className="dropdown-item" href="/manufacturers">Manufacturers</a></li>
              <li><a className="dropdown-item" href="/manufacturers/create">Add a Manufacturer</a></li>
            </ul>
          </div>
          <div className="dropdown">
            <button className="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
            Cars
            </button>
            <ul className="dropdown-menu">
              <li><a className="dropdown-item" href="/automobiles">Cars</a></li>
              <li><a className="dropdown-item" href="/automobiles/create">Add a Car</a></li>
            </ul>
          </div>
          <div className="dropdown">
            <button className="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
            Models
            </button>
            <ul className="dropdown-menu">
              <li><a className="dropdown-item" href="/models">Models</a></li>
              <li><a className="dropdown-item" href="/models/create">Add a Models</a></li>
            </ul>
          </div>
          </ul>

        </div>
      </div>
    </nav>
  )
}

export default Nav;

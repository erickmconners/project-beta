import React, { useState, useEffect } from 'react';

function ListCustomer() {
    const [customer, setCustomer] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/customers/';
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                console.log('API Response:', data);
                setCustomer(data.Customer);
            }
        } catch (error) {
            console.log('Fetch Error:', error);
        }
    };
    useEffect(()=> {
        fetchData();
    }, [])
    return (
        <div>
            <h1>Customers</h1>
        <table className="table table-hover">
            <thead className="table-warning">
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Address</th>
                    <th scope="col">Phone Number</th>
                </tr>
            </thead>
            <tbody className="table-group-divider">
                {customer.map(customers => {
                    return (
                        <tr key={customers.id}>
                            <td>{customers.first_name} {customers.last_name}</td>
                            <td>{customers.address}</td>
                            <td>{customers.phone_number}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
    );

}

export default ListCustomer

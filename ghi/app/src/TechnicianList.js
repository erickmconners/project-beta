import React, { useEffect, useState } from 'react';

function TechnicianList() {
  const [technicians, setTechnician] = useState([]);

  async function fetchTechnician() {
    const response = await fetch('http://localhost:8080/api/technicians/');

    if (response.ok) {
      const parsedJson = await response.json();
      setTechnician(parsedJson.technicians);
    }
  }

  useEffect(() => {
    fetchTechnician();
  }, []);

  return (
    <div>
      <h1>Technicians</h1>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Employee Id</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {technicians.map((technician) => {
          return (
            <tr key={technician.id}>
              <td>{technician.first_name}</td>
              <td>{technician.last_name}</td>
              <td>{technician.employee_id}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
    </div>
  );
}

export default TechnicianList;

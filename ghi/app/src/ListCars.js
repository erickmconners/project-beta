import React, { useState, useEffect } from 'react';

function ListCars() {
    const [cars, setCars] = useState([]);

    useEffect(()=> {
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/automobiles/';
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setCars(data.autos);
            }
        } catch (error) {
            console.log('Fetch Error:', error);
        }
    };
    fetchData();
    }, [])

    return (
        <div>
        <h1>Cars</h1>
        <table className="table table-hover">
            <thead className="table-warning">
                <tr>
                    <th scope="col">Model</th>
                    <th scope="col">Color</th>
                    <th scope="col">Year</th>
                    <th scope="col">VIN</th>
                    <th scope="col">Maufacturer</th>
                </tr>
            </thead>
            <tbody className="table-group-divider">
                {cars.map(s => {
                    return (
                        <tr key={s.id}>
                            <td>{s.model.name}</td>
                            <td>{s.color}</td>
                            <td>{s.year}</td>
                            <td>{s.vin}</td>
                            <td>{s.model.manufacturer.name}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
    );
}

export default ListCars;

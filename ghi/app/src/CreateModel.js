import React, { useState, useEffect } from 'react'

function CreateModel() {
    const [model, setModel] = useState('')
    const [picture, setPicture] = useState('')
    const [manufacturer, setManufacturer] = useState('')
    const [manufacturers, setManufacturers] = useState([])


    useEffect(() => {
        const fetchData = async () => {
            const url = 'http://localhost:8100/api/manufacturers/'
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setManufacturers(data.manufacturers)
            }
        }
    fetchData();
    }, []);

    function handleModelChange(e) {
        setModel(e.target.value)
    }
    function handlePictureChange(e) {
        setPicture(e.target.value)
    }
    function handleManufacturerChange(e) {
        setManufacturer(e.target.value)
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const data = {
        name: model,
        picture_url: picture,
        manufacturer_id: manufacturer,
        };

        const response = await fetch('http://localhost:8100/api/models/', {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        });
        if (response.ok) {
            setModel('');
            setPicture('');
            setManufacturer('');
        }
    }
    return(
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a Model</h1>
                <form onSubmit={handleSubmit} id="create-car-form">
                    <div className="form-floating mb-3">
                        <input value={model} onChange={handleModelChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                        <label htmlFor="car">Model Name...</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={picture} onChange={handlePictureChange} placeholder="Year" required type="text" name="year" id="year" className="form-control" />
                        <label htmlFor="year">Picture URL...</label>
                    </div>
                    <div className="mb-3">
                        <select value={manufacturer} onChange={handleManufacturerChange} name="model" id="model" className="form-select" required>
                            <option value="">Choose a manufacturer...</option>
                            {manufacturers.map(m => {
                                return (<option value={m.id} key={m.id}>{m.name}</option>)
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
    )
}
export default CreateModel;

import React, { useState, useEffect } from 'react';

function NewSale() {
  const [vins, setVins] = useState([]);
  const [salespersons, setSalespersons] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [price, setPrice] = useState('');
  const [vin, setVin] = useState('');
  const [salesperson, setSalesperson] = useState('');
  const [customer, setCustomer] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      try {
        const vinResponse = await fetch('http://localhost:8090/api/automobiles/');
        const customerResponse = await fetch('http://localhost:8090/api/customers/');
        const salespersonResponse = await fetch('http://localhost:8090/api/salespeople/');

        if (vinResponse.ok && customerResponse.ok && salespersonResponse.ok) {
          const carData = await vinResponse.json();
          const customerData = await customerResponse.json();
          const salespersonData = await salespersonResponse.json();

          setVins(carData.Automobiles);
          setCustomers(customerData.Customer);
          setSalespersons(salespersonData.Salesperson);
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []);

  function handleVinChange(e) {
    setVin(e.target.value)
  }
  function handleSalespersonChange(e) {
      setSalesperson(e.target.value)
  }
  function handleCustomerChange(e) {
      setCustomer(e.target.value)
  }
  function handlePriceChange(e) {
      setPrice(e.target.value)
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {
      automobile: vin,
      salesperson: salesperson,
      customer: customer,
      price: price,
    };
    console.log("message:", data)
    const response = await fetch('http://localhost:8090/api/sales/', {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    });

    if (response.ok) {
      setVin('');
      setSalesperson('');
      setCustomer('');
      setPrice('');
    }
  };

return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a new Sale</h1>
          <form onSubmit={handleSubmit} id="create-sale-form">
            <div className="mb-3">
              <select value={vin} onChange={handleVinChange} required name="automobile" id="automobile" className="form-select">
                <option value="">Choose an Automobile VIN...</option>
                {vins.map(v => {
                  return (<option key={v.vin} value={v.vin}>
                    {v.vin}
                  </option>)
                  })}
              </select>
            </div>
            <div className="mb-3">
              <select value={salesperson} onChange={handleSalespersonChange} required name="salesperson" id="salesperson" className="form-select">
                <option value="">Choose a Salesperson...</option>
                {salespersons.map(s => {
                  return (<option key={s.id} value={s.id}>
                    {s.first_name} {s.last_name}
                  </option>)
                })}
              </select>
            </div>
            <div className="mb-3">
              <select value={customer} onChange={handleCustomerChange} required name="customer" id="customer" className="form-select">
                <option value="">Choose a Customer...</option>
                {customers.map(c => {
                  return (<option key={c.id} value={c.id}>
                    {c.first_name} {c.last_name}
                  </option>)
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input value={price} onChange={handlePriceChange} placeholder="Price" required type="text" name="price" className="form-control" />
              <label htmlFor="price">Price</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default NewSale;

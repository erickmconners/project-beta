from .models import Salesperson, Customer, Sale, AutomobileVO
from django.views.decorators.http import require_http_methods
from django.shortcuts import get_object_or_404
from common.json import ModelEncoder
from django.http import JsonResponse
import json


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "salesperson",
        "customer",
        "price",
        "automobile",
    ]
    encoders = {
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
        "automobile": AutomobileVOEncoder(),
    }

    def get_extra_data(self, o):
        return {"vin": o.automobile.vin}


@require_http_methods(["GET", "POST"])
def list_automobiles(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"Automobiles": automobiles},
            encoder=AutomobileVOEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        automobiles = AutomobileVO.objects.create(**content)
        return JsonResponse(
            automobiles,
            encoder=AutomobileVOEncoder,
            safe=False
        )


@require_http_methods(["GET", "POST"])
def list_salesperson(request):
    if request.method == "GET":
        salespersons = Salesperson.objects.all()
        return JsonResponse(
            {"Salesperson": salespersons},
            encoder=SalespersonEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        salespersons = Salesperson.objects.create(**content)
        return JsonResponse(
            salespersons,
            encoder=SalespersonEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def delete_salesperson(request, id):
    if request.method == "DELETE":
        salespersons = get_object_or_404(Salesperson, id=id)
        salespersons.delete()
        return JsonResponse(
            {"Message": f"Salesperson {id} is deleted."}
        )


@require_http_methods(["GET", "POST"])
def list_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"Customer": customers},
            encoder=CustomerEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        customers = Customer.objects.create(**content)
        return JsonResponse(
            customers,
            encoder=CustomerEncoder,
            safe=False
        )


@require_http_methods(["DELETE"])
def delete_customer(request, id):
    if request.method == "DELETE":
        customers = get_object_or_404(Customer, id=id)
        customers.delete()
        return JsonResponse(
            {"Message": f"Customer {id} is deleted."}
        )


@require_http_methods(["GET", "POST"])
def list_sale(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"Sale": sales},
            encoder=SaleEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
    try:
        new_salesperson = Salesperson.objects.get(employee_id=content["salesperson"])
        content["salesperson"] = new_salesperson
    except Salesperson.DoesNotExist:
        return JsonResponse(
            {"Message": "Fix Salesperson"},
            status=400,
        )
    try:
        new_customer = Customer.objects.get(phone_number=content["customer"])
        content["customer"] = new_customer
    except Customer.DoesNotExist:
        return JsonResponse(
            {"Message": "Fix Customer"},
            status=400,
        )
    try:
        new_automobile = AutomobileVO.objects.get(vin=content["automobile"])
        content["automobile"] = new_automobile
    except AutomobileVO.DoesNotExist:
        return JsonResponse(
            {"Message": "Fix Automobile"},
            status=400,
        )
    sale = Sale.objects.create(**content)
    sale.automobile.sold = True
    sale.automobile.save()
    return JsonResponse(
        sale,
        encoder=SaleEncoder,
        safe=False,
    )


@require_http_methods(["DELETE"])
def delete_sale(request, id):
    if request.method == "DELETE":
        sales = get_object_or_404(Sale, id=id)
        sales.delete()
        return JsonResponse(
            {"Message": f"Sale {id} is deleted."}
        )

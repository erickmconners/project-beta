from django.urls import path
from .views import (
   delete_salesperson,
   list_automobiles,
   list_salesperson,
   delete_customer,
   list_customer,
   delete_sale,
   list_sale,
)

urlpatterns = [
    path("salespeople/<int:id>/", delete_salesperson, name="delete_salesperson"),
    path("customers/<int:id>/", delete_customer, name="delete_customer"),
    path("automobiles/", list_automobiles, name="list_automobiles"),
    path("salespeople/", list_salesperson, name="list_salesperson"),
    path("sales/<int:id>/", delete_sale, name="delete_sale"),
    path("customers/", list_customer, name="list_customer"),
    path("sales/", list_sale, name="list_sale"),
]
